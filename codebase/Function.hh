/*
	Copyright © 2017 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the spaghetti
    distribution for more details.
*/

//
// Created by nestal on 2/23/17.
//

#pragma once

#include "EntityVec.hh"
#include "TypeRef.hh"

#include "libclx/Type.hh"
#include "libclx/SourceRange.hh"

#include <boost/iterator/indirect_iterator.hpp>
#include <boost/range/iterator_range_core.hpp>

namespace libclx {
class Cursor;
}

namespace codebase {

class Variable;

class Function : public EntityVec
{
public:
	using param_iterator            = boost::indirect_iterator<std::vector<Variable*>::const_iterator>;

public:
	Function(const libclx::Cursor& first_seen, const EntityVec *parent);
	Function(const Function&) = delete;
	Function(Function&&) = default;
	Function& operator=(const Function&) = delete;
	Function& operator=(Function&&) = default;
	
	void Visit(const libclx::Cursor& self);
	
	EntityType Type() const override;
	libclx::SourceLocation Location() const override;
	TypeRef ReturnType() const;
	
	void CrossReference(EntityMap *map) override;
	void InstantiateImmediateTypes(EntityMap *map) override ;
	
	std::string UML() const override;
	
	boost::iterator_range<param_iterator> Parameters() const;
	
private:
	libclx::SourceLocation  m_definition;
	TypeRef                 m_return_type;
		
	std::vector<Variable*>  m_args;
};
	
} // end of namespace
