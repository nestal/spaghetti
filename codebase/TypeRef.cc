/*
	Copyright © 2017 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the spaghetti
    distribution for more details.
*/

//
// Created by nestal on 3/18/17.
//

#include "TypeRef.hh"
#include "ClassTemplate.hh"

#include "libclx/Cursor.hh"
#include "libclx/Type.hh"

#include <boost/throw_exception.hpp>

#include <ostream>
#include <cassert>
#include <deque>

namespace codebase {

TypeRef::TypeRef(CXTypeKind kind) : TypeRef{{}, kind}
{
}

TypeRef::TypeRef(const std::string& base, CXTypeKind kind) :
	m_kind{kind},
	m_base_id{base},
	m_name{libclx::Type::KindSpelling(m_kind)}
{
}

TypeRef::TypeRef(const libclx::Cursor& cursor)
{
	libclx::Cursor referenced;
	
	std::deque<libclx::Cursor> template_definitions;
	cursor.Visit([&template_definitions, &referenced](libclx::Cursor child, libclx::Cursor)
	{
		if (child.Kind() == CXCursor_TemplateRef)
			template_definitions.push_back(child.Referenced());
		
		else if (child.Kind() == CXCursor_TypeRef && template_definitions.empty())
			referenced = child.Referenced();
	});
	
	auto actual = cursor.Type();
	
	// use the underlying types for aliased types
	if (cursor.Kind() == CXCursor_TypeAliasDecl ||
		cursor.Kind() == CXCursor_TypedefDecl)
		actual = cursor.TypedefUnderlying();
		
	// for functions, we are interested in its return type
	else if (cursor.Kind() == CXCursor_FunctionDecl)
		actual = cursor.ResultType();
			
	m_kind = actual.Kind();
	
	if (!template_definitions.empty())
		MatchTemplates(*this, actual, template_definitions);
	else
	{
		m_base_id = referenced.USR();
		m_name    = referenced.Spelling();
	}
	
	if (!IsValid())
		BOOST_THROW_EXCEPTION(InvalidID{});
}

TypeRef::TypeRef(const libclx::Type& type)
{
	std::deque<libclx::Cursor> d;
	MatchTemplates(*this, type, d);
}

void TypeRef::MatchTemplates(TypeRef& result, const libclx::Type& type, std::deque<libclx::Cursor>& template_definitions)
{
	result.m_kind    = type.Kind();
	result.m_base_id = type.Declaration().USR();
	result.m_name    = type.Spelling();
	
	if (type.NumTemplateArguments() > 0 && !template_definitions.empty())
	{
		result.m_temp_id = template_definitions.front().USR();
		template_definitions.pop_front();
		
		for (auto&& arg : type.TemplateArguments())
		{
			TypeRef temp_arg;
			MatchTemplates(temp_arg, arg, template_definitions);
			result.m_temp_args.push_back(std::move(temp_arg));
		}
	}
}

const std::string& TypeRef::ID() const
{
	return m_base_id;
}

const std::vector<TypeRef>& TypeRef::TempArgs() const
{
	return m_temp_args;
}

TypeRef& TypeRef::AddTempArgs(TypeRef&& arg)
{
	m_temp_args.push_back(std::move(arg));
	return *this;
}

bool TypeRef::operator==(const TypeRef& ref) const
{
	return m_kind == ref.m_kind &&
		(
			// for record types (i.e. class/struct/union), need to compare the IDs of the
			// class/struct/union as well
			(m_kind != CXType_Record) ||
			(m_base_id == ref.m_base_id && m_temp_id == ref.m_temp_id && m_temp_args == ref.m_temp_args)
		);
}

bool TypeRef::operator!=(const TypeRef& ref) const
{
	return !operator==(ref);
}

// for convenience
bool operator==(const TypeRef& type, const std::string& id)
{
	return type.ID() == id;
}

bool operator!=(const TypeRef& type, const std::string& id)
{
	return type.ID() != id;
}

bool operator==(const std::string& id, const TypeRef& type)
{
	return type.ID() == id;
}

bool operator!=(const std::string& id, const TypeRef& type)
{
	return type.ID() != id;
}

bool TypeRef::IsTemplate() const
{
	return !m_temp_id.empty();
}

const std::string& TypeRef::TemplateID() const
{
	return m_temp_id;
}

TypeRef& TypeRef::SetTemplateID(const std::string& id)
{
	m_temp_id = id;
	return *this;
}

TypeRef& TypeRef::SetTemplate(const std::string& temp_id, const std::vector<TypeRef>& args)
{
	m_temp_id = temp_id;
	m_temp_args = args;
	return *this;
}

const std::string& TypeRef::Name() const
{
	return m_name;
}

TypeRef& TypeRef::SetName(std::string&& name)
{
	m_name = std::move(name);
	return *this;
}

TypeRef& TypeRef::SetName(const std::string& name)
{
	m_name = name;
	return *this;
}

CXTypeKind TypeRef::Kind() const
{
	return m_kind;
}

bool TypeRef::IsValid() const
{
	return m_kind != CXType_Record || !m_base_id.empty();
}

bool TypeRef::IsAlias() const
{
	return m_kind == CXType_Typedef;
}

std::ostream& operator<<(std::ostream& os, const TypeRef& ref)
{
	os << ref.ID() << '(' << libclx::Type::KindSpelling(ref.Kind()) << '|' << ref.TemplateID() << '|' << ref.TempArgs().size() << '|';
	
	for (auto&& arg : ref.TempArgs())
		os << arg << ",";
	
	return os << ')';
}

} // end of namespace
