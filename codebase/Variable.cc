/*
	Copyright © 2017 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the spaghetti
    distribution for more details.
*/

//
// Created by nestal on 2/4/17.
//

#include "Variable.hh"

#include "DataType.hh"
#include "EntityMap.hh"
#include "EntityType.hh"
#include "TypeAlias.hh"
#include "TypeReplacer.hh"

#include "libclx/Cursor.hh"

#include <iostream>
#include <boost/throw_exception.hpp>

namespace codebase {

Variable::Variable(const libclx::Cursor& field, const EntityVec *parent) :
	LeafEntity{field.Spelling(), field.USR(), parent},
	m_location{field.Location()},
	m_type{field}
{
	if (ID().empty())
		BOOST_THROW_EXCEPTION(TypeRef::InvalidID{});
	
	assert(m_type.IsValid());
}

Variable::Variable(const Variable& other, const EntityVec *parent) :
	LeafEntity{other.Name(), other.ID(), parent},
	m_location{other.Location()},
	m_type{other.m_type}
{
	assert(!ID().empty());
	assert(m_type.IsValid());
}

EntityType Variable::Type() const
{
	return EntityType::variable;
}

std::ostream& operator<<(std::ostream& os, const Variable& c)
{
	return os << c.DisplayType() << " " << c.Name();
}

libclx::SourceLocation Variable::Location() const
{
	return m_location;
}

std::string Variable::UML() const
{
	return Name() + " : " + DisplayType();
}

void Variable::CrossReference(EntityMap *map)
{
	const auto* entity = map->Find(m_type.ID());
	if (entity && entity->Type() == EntityType::type_alias)
		entity = dynamic_cast<const TypeAlias*>(entity)->Resolve(map);
	
	m_data_type = dynamic_cast<const DataType*>(entity);
}

std::string Variable::DisplayType() const
{
	return m_type.Name();
}

const codebase::TypeRef& Variable::ClassRef() const
{
	return m_type;
}

void Variable::InstantiateImmediateTypes(EntityMap *map)
{
	if (m_type.IsTemplate() && !m_type.ID().empty() && m_type.ID() != m_type.TemplateID())
		map->Instantiate(m_type, IsUsed());
}

std::unique_ptr<Variable> Variable::OnInstantiate(const TypeReplacer *replacer, const EntityVec *parent) const
{
	auto var = std::make_unique<Variable>(*this, parent);
	var->m_type = replacer->Replace(m_type);
	return var;
}

/**
 * \brief Returns the DataType of this variable.
 *
 * For a variable "std::string s", it returns a pointer to std::basic_string<char...>.
 * In other words, alias like typedefs has already been resolved.
 *
 * \return A pointer to the DataType of this variable, or null if it is a build-in type.
 */
const DataType* Variable::ClassType() const
{
//	assert(IsValid());
	return m_data_type;
}

bool Variable::IsValid() const
{
	auto valid = (!m_data_type || m_type.IsAlias() || m_data_type->ID() == m_type.ID());
	if (!valid)
	{
		std::cout << "Invalid variable: " << (void*)m_data_type << " " << m_type << " " << (m_data_type?m_data_type->ID():"") << std::endl;
	}
	
	return valid;
}

} // end of namespace
