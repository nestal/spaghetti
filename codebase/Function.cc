/*
	Copyright © 2017 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the spaghetti
    distribution for more details.
*/

//
// Created by nestal on 2/23/17.
//

#include "Function.hh"
#include "Variable.hh"
#include "EntityType.hh"
#include "EntityMap.hh"

#include "libclx/Cursor.hh"
#include "libclx/Type.hh"

#include <iostream>
#include <sstream>

namespace codebase {

Function::Function(const libclx::Cursor& first_seen, const EntityVec *parent) :
	EntityVec{first_seen.Spelling(), first_seen.USR(), parent},
	m_definition{first_seen.Location()},
	m_return_type{first_seen}
{
}

libclx::SourceLocation Function::Location() const
{
	return m_definition;
}

EntityType Function::Type() const
{
	return EntityType::function;
}

void Function::Visit(const libclx::Cursor& self)
{
	assert(Parent());
	m_definition = self.Location();
	
	SetUsed(Parent()->IsUsed());
	
	self.Visit([this](libclx::Cursor cursor, libclx::Cursor)
	{
		try
		{
			switch (cursor.Kind())
			{
			case CXCursor_ParmDecl:
				AddUnique(m_args, cursor.USR(), cursor, this);
				break;
			
			default:
//				std::cout << Name() << " " <<  cursor.Spelling() << ' ' << cursor.KindSpelling() << std::endl;
				break;
			}
		}
		catch (TypeRef::InvalidID&)
		{
			
		}
	});
	
	// mark self and all children as used, after creating the children
	if (self.IsDefinition() && self.Location().IsFromMainFile())
		SetUsed();
}

void Function::CrossReference(EntityMap *)
{
}

std::string Function::UML() const
{
	std::ostringstream oss;
	oss << Name() << "(";
//	for (auto& arg : m_args)
//		oss << arg << ", ";
	oss << ") : " << m_return_type.Name();
	return oss.str();
}

TypeRef Function::ReturnType() const
{
	return m_return_type;
}

void Function::InstantiateImmediateTypes(EntityMap *map)
{
	// This may sound strange, but Functions does not have immediate types.
	// The return value is a CXType (libclx::Type), we can't get the USR of
	// the actual class template that generates it.
	if (m_return_type.IsTemplate() && !m_return_type.ID().empty() && m_return_type.ID() != m_return_type.TemplateID())
		map->Instantiate(m_return_type, IsUsed());
}

boost::iterator_range<Function::param_iterator>
Function::Parameters() const
{
	return {m_args.begin(), m_args.end()};
}
	
} // end of namespace
