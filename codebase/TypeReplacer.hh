/*
	Copyright © 2017 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the spaghetti
    distribution for more details.
*/

//
// Created by nestal on 3/29/17.
//

#pragma once

namespace codebase {

class TypeRef;

class TypeReplacer
{
public:
	virtual ~TypeReplacer() = default;
	
	virtual const TypeRef& Replace(const TypeRef& type) const = 0 ;
};
	
} // end of namespace
