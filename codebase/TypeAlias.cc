/*
	Copyright © 2017 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the spaghetti
    distribution for more details.
*/

//
// Created by nestal on 3/26/17.
//

#include "TypeAlias.hh"

#include "EntityType.hh"
#include "EntityMap.hh"
#include "DataType.hh"

#include "libclx/Cursor.hh"

#include <unordered_set>

namespace codebase {

TypeAlias::TypeAlias(const libclx::Cursor& cursor, const EntityVec *parent) :
	LeafEntity{cursor.Spelling(), cursor.USR(), parent},
	m_location{cursor.Location()},
	m_dest{cursor}
{
	assert(
		cursor.Kind() == CXCursor_TypeAliasDecl ||
		cursor.Kind() == CXCursor_TypedefDecl
	);
	assert(m_dest.IsValid());
}

const TypeRef& TypeAlias::Dest() const
{
	return m_dest;
}

EntityType TypeAlias::Type() const
{
	return EntityType::type_alias;
}

std::string TypeAlias::DisplayType() const
{
	return "typedef";
}

libclx::SourceLocation TypeAlias::Location() const
{
	return m_location;
}

std::string TypeAlias::UML() const
{
	return Name();
}

const Entity *TypeAlias::Resolve(EntityMap *map) const
{
	std::unordered_set<const Entity*> past;
	
	const Entity *entity = this;
	while (entity && entity->Type() == EntityType::type_alias && past.find(entity) == past.end())
	{
		past.insert(entity);
		
		auto alias = dynamic_cast<const TypeAlias*>(entity);
		auto dest  = alias->Dest();
		
		entity =
			(dest.IsTemplate() && !dest.ID().empty() && dest.ID() != dest.TemplateID()) ?
				map->Instantiate(dest, IsUsed()) :
				map->Find(dest.ID());
	}
	return entity;
}
	
} // end of namespace
