/*
	Copyright © 2017 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the spaghetti
    distribution for more details.
*/

//
// Created by nestal on 3/29/17.
//

#include "Fixture.hh"

#include "codebase/DataType.hh"
#include "codebase/Variable.hh"
#include "codebase/EntityMap.hh"

#include <gtest/gtest.h>

using namespace codebase;

class NestedClassInstantiationTest : public ut::Fixture
{
protected:
	NestedClassInstantiationTest() : Fixture{"instantiate_nested_class.cc"}
	{
		
	}
};

TEST_F(NestedClassInstantiationTest, Test1)
{
	auto apples = dynamic_cast<const DataType*>(m_map.FindByName("CondNested<Apple>"));
	ASSERT_TRUE(apples);
//	std::cout << "apples = " << (void*)apples << " " << typeid(*apples).name() << std::endl;
	
//	auto in_apples = dynamic_cast<const DataType*>(m_map.FindByName("CondNested<Apple>::Impl"));
//	ASSERT_TRUE(in_apples);
}
