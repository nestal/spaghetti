/*
	Copyright © 2017 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the spaghetti
    distribution for more details.
*/

//
// Created by nestal on 3/29/17.
//

struct Apple {};

template <typename T1>
struct Temp
{
};

Temp<int> SomeNonTemplateFunction(int);

template <typename T>
class CondNested
{
public:
	struct Impl
	{
		T   *ptr;
	};

	Temp<T> Func(int);
	
private:
	Impl impl;
};

class Instantce
{
public:
	CondNested<Apple> apples;
	
	Instantce()
	{
		apples.Func(1);
	}
};

int main()
{
}
