/*
	Copyright © 2017 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the spaghetti
    distribution for more details.
*/

//
// Created by nestal on 3/30/17.
//

void FunctionReturnVoid();

typedef int Int;

Int FunctionReturnTypeDef(int i);

void FunctionWithUnamedArg(int);

template <typename T>
class Cond
{
public:
	T t;
};

struct String {};

Cond<String> FunctionReturnTemplateInst(Cond<int> cond, String s, int i);
