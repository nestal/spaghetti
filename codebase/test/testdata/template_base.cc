int global_var = 0;

struct String {};
struct String2 {};

template <typename T, typename T2>
struct Temp2 { T t; T2 t2;};

template <typename T>
struct Temp1 { T t; };

class Base4
{
public:
	Base4() = default;
};

template <typename T>
class Container
{
public:
	T *m_array;
	unsigned size;
};

class Base
{
private:
	Temp2<Temp2<String, int>, Temp1<String2>> m_temp;
	Base4 m_b4;
	Container<String2> m_strs;
	
public:
	virtual ~Base() = default;
	virtual void Func() = 0;
};

template <typename BaseType>
class RecursiveBase : public BaseType, public Base4
{
public:
	virtual void SomeFunc()
	{
		// prevent optimizer to remove the function
		global_var++;
	}
};

class Base2
{
public:
	Base2() = default;
};

class Base3
{
public:
	Base3() = default;
};

class Derived : public RecursiveBase<Base>, public Base2, public Base3
{
public:
	Derived() = default;

	void Func() override {}
};

int main()
{
	Derived d;
	return 0;
}
