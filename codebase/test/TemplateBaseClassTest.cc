/*
	Copyright © 2017 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the spaghetti
    distribution for more details.
*/

//
// Created by nestal on 3/11/17.
//

#include "Fixture.hh"

#include "codebase/DataType.hh"
#include "codebase/Variable.hh"
#include "codebase/EntityMap.hh"

#include <gtest/gtest.h>

using namespace codebase;

class TemplateBaseClassTest : public ut::Fixture
{
protected:
	TemplateBaseClassTest() : Fixture{"template_base.cc"}
	{
		
	}
};

TEST_F(TemplateBaseClassTest, Test_base_class)
{
	auto derived = dynamic_cast<const DataType *>(m_subject.Map().FindByName("Derived"));
	ASSERT_EQ("Derived", derived->Name());
	
	// base should be c:@S@RecursiveBase>#$@S@Base, but we need to fix it by
	// differentiating between a template and its instantiation
	std::vector<TypeRef> base{
		TypeRef{"c:@S@RecursiveBase>#$@S@Base", CXType_Unexposed}.SetTemplateID("c:@ST>1#T@RecursiveBase").AddTempArgs(TypeRef{"c:@S@Base"}),
		TypeRef{"c:@S@Base2"},
		TypeRef{"c:@S@Base3"}
	};
	ASSERT_EQ(base, derived->BaseClasses());
	
	auto temp_base = m_subject.Map().Find("c:@ST>1#T@RecursiveBase");
	
	ASSERT_TRUE(temp_base);
	ASSERT_EQ("RecursiveBase<BaseType>", temp_base->Name());
	
	auto inst_base = m_subject.Map().TypedFind<DataType>("c:@S@RecursiveBase>#$@S@Base");
	ASSERT_TRUE(inst_base);
	
	std::vector<TypeRef> bbase{
		TypeRef{"c:@S@Base"},
		TypeRef{"c:@S@Base4"}
	};
	ASSERT_EQ(bbase, inst_base->BaseClasses());
}

TEST_F(TemplateBaseClassTest, Test_template_fields)
{
	auto base = dynamic_cast<const DataType*>(m_subject.Map().FindByName("Base"));
	auto base_fields = base->Fields();
	
	ASSERT_EQ(3, base_fields.size());
	auto& base_field = base_fields[0];
	
	auto field_type = m_subject.Map().TypedFind<DataType>(base_field.ClassRef().ID());
	ASSERT_TRUE(field_type);
	ASSERT_EQ("Temp2<Temp2<String, int>, Temp1<String2> >", field_type->Name());
	
	auto temp2_fields = field_type->Fields();
	ASSERT_EQ(2, temp2_fields.size());
	ASSERT_EQ(CXType_Record, temp2_fields.front().ClassRef().Kind());
	ASSERT_EQ("Temp2<String, int>", temp2_fields.front().ClassRef().Name());
}

TEST_F(TemplateBaseClassTest, Test_Container_template)
{
	auto base = dynamic_cast<const DataType*>(m_subject.Map().FindByName("Base"));
	auto base_fields = base->Fields();
	
	ASSERT_EQ(3, base_fields.size());
	auto& strs = base_fields[2];
	
	auto cond_str = m_subject.Map().TypedFind<DataType>(strs.ClassRef().ID());
	ASSERT_TRUE(cond_str);
	ASSERT_EQ("Container<String2>", cond_str->Name());
	
	auto cond_fields = cond_str->Fields();
	ASSERT_EQ(2, cond_fields.size());
	
	auto pstr = cond_fields[0];
	ASSERT_EQ("String2", pstr.ClassRef().Name());
	
	auto str_class = dynamic_cast<const DataType*>(m_subject.Map().FindByName("String2"));
	ASSERT_TRUE(str_class);
	ASSERT_TRUE(str_class->IsUsedInMember(*cond_str));
}
