/*
	Copyright © 2017 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the spaghetti
    distribution for more details.
*/

//
// Created by nestal on 3/30/17.
//

#include <gtest/gtest.h>

#include "Fixture.hh"

#include "codebase/DataType.hh"
#include "codebase/TypeRef.hh"
#include "codebase/EntityMap.hh"
#include "codebase/Function.hh"
#include "codebase/Variable.hh"

using namespace codebase;

class FunctionTest : public ut::Fixture
{
protected:
	FunctionTest() : Fixture{"function_test.cc"}
	{
	
	}
};

TEST_F(FunctionTest, Test_function_return_void)
{
	auto func = dynamic_cast<const Function*>(m_map.FindByName("FunctionReturnVoid"));
	ASSERT_TRUE(func);
	
	auto rtype = func->ReturnType();
	ASSERT_EQ(CXType_Void, rtype.Kind());
	ASSERT_TRUE(rtype.IsValid());
}

TEST_F(FunctionTest, Test_function_return_temp_inst)
{
	auto func = dynamic_cast<const Function*>(m_map.FindByName("FunctionReturnTemplateInst"));
	ASSERT_TRUE(func);
	
	auto rtype = func->ReturnType();
	ASSERT_EQ(CXType_Unexposed, rtype.Kind());
	ASSERT_TRUE(rtype.IsValid());
	ASSERT_EQ("Cond<String>", rtype.Name());
	ASSERT_EQ("c:@ST>1#T@Cond", rtype.TemplateID());
	ASSERT_TRUE(rtype.IsTemplate());
	
	auto rtype_entity = m_map.TypedFind<DataType>(rtype.ID());
	ASSERT_TRUE(rtype_entity);
	
	auto fields = rtype_entity->Fields();
	ASSERT_EQ(1, fields.size());
	
	// assert function parameters
	auto params = func->Parameters();
	ASSERT_EQ(3, params.size());
	
	// first parameter is Cond<int>
	ASSERT_EQ("cond", params[0].Name());
	auto&& cond_int = params[0].ClassRef();
	ASSERT_TRUE(cond_int.IsTemplate());
	ASSERT_EQ("Cond<int>", cond_int.Name());
	ASSERT_EQ(CXType_Unexposed, cond_int.Kind());
	ASSERT_EQ(1, cond_int.TempArgs().size());
	ASSERT_EQ(CXType_Int, cond_int.TempArgs().at(0).Kind());
	
	// second parameter is String
	ASSERT_EQ("s", params[1].Name());
	auto&& str = params[1].ClassRef();
	ASSERT_FALSE(str.IsTemplate());
	ASSERT_EQ(CXType_Record, str.Kind());
	ASSERT_EQ("String", str.Name());
	
	// third parameter is int
	ASSERT_EQ("i", params[2].Name());
	auto&& int_type = params[2].ClassRef();
	ASSERT_FALSE(int_type.IsTemplate());
	ASSERT_EQ(CXType_Int, int_type.Kind());
	ASSERT_EQ("", int_type.Name());
}
